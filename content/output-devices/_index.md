+++
title = "Week 11: Output Devices"
+++

Here you can find documentation of the Output Devices group assignment which maps to the following Fab Academy assignment weeks.

- [Output Devices](http://academy.cba.mit.edu/classes/output_devices/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Measure the power consumption of an output device
- Document your work (in a group or individually)

---

### Initial Responsibilities

- ?: **Anssi**
- ?: **Jasmine**
- ?: **Ranjit**
- ATtiny1614 + A4953 Stepper Drivers + A Stepper Motor : **Wan-Ting**

---

## Build

### ATtiny1614 + A4953 Stepper Drivers + A Stepper Motor ([Wan-Ting Hsieh's Documentation](https://fabacademy.wantinghsieh.com/assignment/11-output-devices))

### ? (Jasmine)

### ? (Ranjit)
