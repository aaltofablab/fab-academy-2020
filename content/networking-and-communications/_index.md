+++
title = "Week 13: Networking and Communications"
+++

Here you can find documentation of the Networking and Communications group assignment which maps to the following Fab Academy assignment weeks.

- [Networking and Communications](http://academy.cba.mit.edu/classes/networking_communications/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Send a message between two projects

---

### Initial Responsibilities

- ESP32 Wireless One-Way Communication: **Wan-Ting**
- ?: **Anssi**
- Arduino RX/TX Serial Communication: **Jasmine**
- ?: **Ranjit**

---

### Arduino RX/TX Serial Communication: **Jasmine**

---

### [Wan-Ting: ESP32 Wireless Two-Way Communication Project - Built-in Touch Sensors + Conductive Fabrics + LED](http://fabacademy.wantinghsieh.com/assignment/13-networking-and-communications)
![ESP32_two_way_touch_2_web.jpg](./images/ESP32_two_way_touch_2_web.jpg)

### Build

#### Pin Connection

Since both ESP32 act as a sender and a receiver at the same time, I connected a LED and one piece of conductice fabrics to the same pins of each ESP32 for easily programming. (Note. Make sure to connect the conductive fabric to **Touch Sensor Pins** introduced [here](http://fabacademy.wantinghsieh.com/assignment/13-networking-and-communications#pin-functions).)

![ESP32_two_way_touch.jpg](./images/ESP32_two_way_touch.jpg)

### Interaction

The **green** LED was lighted up while I touched the conductive fabric connected to the opposite ESP32.

![ESP32_two_way_touch_1_web.jpg](./images/ESP32_two_way_touch_1_web.jpg)

While I pressed the conductive fabric connected to the other ESP32, it lighted up the **yellow** LED connected to the previous one.

![ESP32_two_way_touch_3_web.jpg](./images/ESP32_two_way_touch_3_web.jpg)

### Programming

**How to set up the programming environment for ESP32 can be found from [the above section of Week 13: Networking and Communications](https://fabacademy.wantinghsieh.com/assignment/13-networking-and-communications#programming).**

For two-way communication, both ESP32 boards use the same code but **different MAC addresses** to work as a sender and a receiver simultaneously. I followed the [ESP-NOW Two-Way Communication Between ESP32 Boards](https://randomnerdtutorials.com/esp-now-two-way-communication-esp32/) tutorial and adjusted the code the form the following interaction.

![ESP-NOW-send-sensor-readings-project-overview.png](./images/ESP-NOW-send-sensor-readings-project-overview.png)

```arduino
#include <esp_now.h>
#include <WiFi.h>
#include <analogWrite.h>

// REPLACE WITH the MAC Address of THE OPPOSITE BOARD
uint8_t broadcastAddress[] = {0x24, 0x6F, 0x28, 0x7A, 0xA0, 0xF4};  // ESP32 #1
//uint8_t broadcastAddress[] = {0x24, 0x6F, 0x28, 0x7A, 0xB1, 0x78}; // ESP32 #2

#define PIEZO 4 // Touch Sensor pin: analog (0-4095)
#define LED 16 // PWM pin

const int rangeMax = 35;
const int rangeMin = 6;
int sensorValue = 0;        // value read from the touch sensor connected to conductive fabric
int outputValue = 0;        // value output to the PWM (analog out)

int myData; // Sensor State

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("LED Value: ");
  Serial.println(myData);

  // change the analog out value:
  analogWrite(LED, myData);
}


void setup() {
  // Init Serial Monitor
  Serial.begin(115200);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  // Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }

  // Register for a callback function that will be called when data is received
  esp_now_register_recv_cb(OnDataRecv);
}

void loop() {
  getSensorValue();

  // Send message via ESP-NOW
  esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &outputValue, sizeof(outputValue));

  if (result == ESP_OK) {
    Serial.print("Sent with success, Sensor Value: ");
  }
  else {
    Serial.println("Error sending the data");
  }

  Serial.print(sensorValue);
  delay(200);
}

void getSensorValue() {
  sensorValue = touchRead(PIEZO);
  sensorValue = (sensorValue > rangeMax) ? rangeMax : sensorValue;
  // map it to the range of the analog out:
  outputValue = map(sensorValue, rangeMax, rangeMin, 0, 255);
}
```

#### How the code works

The code combines the functions from both **sender** and **receiver** introduced [here](http://fabacademy.wantinghsieh.com/assignment/13-networking-and-communications#esp32-button-sender-1). Except these, I replaced the button whose state can be read with `digitalRead` function with conductive fabrics connected to **Touch Sensors** whose **analog** state can only be read with `touchRead()` function with the value ranging from **0-4095**.

According to this, I first defined which **Touch Sensor** is connected to conductive fabric and called it **PIEZO** pin and then connected the LED to a **PWM** pin which can `analogWrite()` the value ranging from **0-255** instead of 0 or 1 (HIGH or LOW).

```arduino
#define PIEZO 4 // Touch Sensor pin: analog (0-4095)
#define LED 16 // PWM pin

const int rangeMax = 35;
const int rangeMin = 6;
int sensorValue = 0;        // value read from the touch sensor connected to conductive fabric
int outputValue = 0;        // value output to the PWM (analog out)

int myData; // Sensor State
```

Next, I created a function called `getSensorValue()` put in the `loop()` to first `touchRead()` the value from the **Touch Sensor**, mapped the value from **0-4095** to **0-255** and then `analogWrite()` to the LED. Sometimes it is better to narrow down the range of **Touch Sensor** by defining a threshold to fillter out the changing scope.

```arduino
void getSensorValue() {
  sensorValue = touchRead(PIEZO);
  sensorValue = (sensorValue > rangeMax) ? rangeMax : sensorValue;
  // map it to the range of the analog out:
  outputValue = map(sensorValue, rangeMax, rangeMin, 0, 255);
}
```

### End Result

{{< video src="ESP32_two_way_touch_LED_small.mp4" width="800px" >}}
