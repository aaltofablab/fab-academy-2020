# Fab Academy 2020

This project is here to make it easier to collaborate on course issues. You can think of it as the website of a course. Here you can find information about the local shcedule and activities regarding the Fab Academy 2020 course.

Instructor: [Krisjanis Rijnieks](https://rijnieks.com)  
Email: krisjanis.rijnieks [at] aalto [dot] fi

The repository website is built using the [Go Hugo](https://gohugo.io/) engine. Content is stored in the `content` directory as Markdown files, `.gitlab-ci.yml` configuration file describes how GitLab should use Hugo to compile it into HTML format and publish it. Run `hugo serve` locally to test changes made before pushing it to the main repository. 

## [Group Assignment Website](https://aaltofablab.gitlab.io/fab-academy-2020/)

## Schedule

Please follow [the official Fab Academy 2020 schedule](http://fabacademy.org/2020/schedule.html) for global lecture and recitation dates. Locally the weekly structure is as follows.

- Wednesday 16:00: global lecture
- Friday 16:00: local lecture (now via Zoom)
- Monday 16:00: global recitation (every 2 weeks)

## When can I Work at the Lab?

During COVID-19 the lab is open Tue, Wed and Thu from 9:00 to 16:00 if not spcefied otherwise.

## Students

| Name | Repo | Site | Issues |
|---   |---   |---   |----    |
| Jasmine Xie | [repo](https://gitlab.com/bluet87/fab-academy) | [site](https://bluet87.gitlab.io/fab-academy/) | [issues](https://gitlab.com/bluet87/fab-academy/issues) |
| Wan-Ting Hsieh | [repo](https://gitlab.com/cv47522/fab-academy) | [site](http://fabacademy.wantinghsieh.com/) | [issues](https://gitlab.com/cv47522/fab-academy/issues)
| Anssi Alhopuro | [repo](https://gitlab.com/anzibar/fab-academy) | [site](https://anzibar.gitlab.io/fab-academy/) | [issues](https://gitlab.com/anzibar/fab-academy/issues) |
| Solomon Kiflom | [repo](https://gitlab.fabcloud.org/academany/fabacademy/2019/labs/aalto/students/solomon-embafrash) | [site](http://fabacademy.org/2019/labs/aalto/students/solomon-embafrash/) | [issues](https://gitlab.fabcloud.org/academany/fabacademy/2019/labs/aalto/students/solomon-embafrash/issues) |
| Ranjit Menon | [repo](https://gitlab.fabcloud.org/academany/fabacademy/2020/labs/kochi/students/ranjit-menon) | [site](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/) | [issues](https://gitlab.fabcloud.org/academany/fabacademy/2020/labs/kochi/students/ranjit-menon/issues) |

## Documentation

Every participant should have a documentation project hosted on their GitLab account. We use GitLab to maintain consistency and to learn the wizardry of Git. The name of your repository (GitLab project) should be **fab-academy**. Your documentation should end up being a static HTML website. We will cover how to automatically publish it on every `git push` using GitLab CI/CD during the class. The recommended content structure of your documentation website should be as follows.

- About
- Final Project
- Assignments
  + Week 01: Project Management
  + Week 02: Computer-Aided Design
  + Week 03: Computer-Controlled Cutting
  + ...
  + Documentation feature by the normal Kris
  + Another documentation feature by cloud Kris

## Weekly Assignments

Weekly assignments will be posted as per-issue basis. Every weekly assignment will be assigned an issue in this project. Please keep the discussion about weekly assignments in their respective issue pages. Please post general questions in the **General** issue.

- Assignments are checked every week before global lecture on **Wednesdays**.
- Local review and local lecture happens on **Thursdays**.
- New issues for assignments are added on **Fridays**.
