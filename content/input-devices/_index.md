+++
title = "Week 09: Input Devices"
+++

Here you can find documentation of the Input Devices group assignment which maps to the following Fab Academy assignment weeks.

- [Input Devices](http://academy.cba.mit.edu/classes/input_devices/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Probe an input device(s)'s analog and digital signals
- Document your work (in a group or individually)

---

### Initial Responsibilities

- ?: **Anssi**
- ?: **Jasmine**
- ?: **Ranjit**
- IR Receiving Sensor made with an NPN-Phototransistor: **Wan-Ting**

---

## Build

### IR Receiving Sensor made with an NPN-Phototransistor ([Wan-Ting Hsieh's Documentation](https://fabacademy.wantinghsieh.com/assignment/09-input-devices))

### ? (Jasmine)

### ? (Ranjit)
