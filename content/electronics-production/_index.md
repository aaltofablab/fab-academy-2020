+++
title = "Week 04: Electronics Production"
+++

This issue is related to Fab Academy [Week 04: Electronics Production](http://academy.cba.mit.edu/classes/electronics_production/index.html).

---

## TODO

- Characterise the design rules for your PCB production process: document feeds, speeds, plunge rate, depth of cut (traces and outline) and tooling.
- document your work (in a group or individually)

---

## Build

The assignment is documented individually:

- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/assignment/04-electronics-production)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/assignments/week-04/)
- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/assignments/week05/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/week04.html)
