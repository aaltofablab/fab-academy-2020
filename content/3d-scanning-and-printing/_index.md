+++
title = "Week 05: 3D Scanning and Printing"
+++

Here you can find documentation of the 3D Scanning and Printing group assignment which maps to the following Fab Academy assignment weeks.

- [3D Scanning and Printing](http://academy.cba.mit.edu/classes/scanning_printing/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Test the design rules for your printer(s)
- Document your work and explain what are the limits of your printer(s) (in a group or individually)

---

### Initial Responsibilities

- ?: **Anssi**
- ?: **Jasmine**
- ?: **Ranjit**
- Limitation of printing angle: Ultimaker 2+ Extended  **Wan-Ting**

---

## Build

### Limitation of printing angle: Ultimaker 2+ Extended ([Wan-Ting Hsieh's Documentation](https://fabacademy.wantinghsieh.com/assignment/05-3d-scanning-and-printing/))

### ? (Jasmine)

### ? (Ranjit)
