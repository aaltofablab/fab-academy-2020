+++
title = "Week 07: Computer-Controlled Machining"
+++

Here you can find documentation of the Computer-Controlled Machining group assignment which maps to the following Fab Academy assignment weeks.

- [Computer-Controlled Machining](http://academy.cba.mit.edu/classes/computer_machining/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Test runout, alignment, speeds, feeds, and toolpaths for your machine
- Document your work (in a group or individually)

---

## Build

### [Wan-Ting Hsieh's Documentation](https://fabacademy.wantinghsieh.com/assignment/07-computer-controlled-machining)

### ? (Jasmine)

### ? (Ranjit)
