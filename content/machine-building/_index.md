+++
title = "Week 17: Machine Building"
+++

Here you can find documentation of the Machine Building group assignment which maps to the following Fab Academy assignment weeks.

- [Mechanical Design](http://academy.cba.mit.edu/classes/mechanical_design/index.html)
- [Machine Design](http://academy.cba.mit.edu/classes/machine_design/index.html)

There are two weeks scheduled for that,

- Week 38 (14 - 18 Sep 2020)
- Week 39 (21 - 25 Sep 2020)

Both weeks the lab will be open.

- Monday to Friday
- 9:00 - 16:00

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
  - [Personal contribution documentation](https://bluet87.gitlab.io/fab-academy/assignments/week-17/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
  - [Personal contribution documentation](https://fabacademy.wantinghsieh.com/assignment/17-mechanical-and-machine-design)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

## Resources

- [Parts Available](https://docs.google.com/spreadsheets/d/1PE_EjZsktCRB8p5HlJDYPPXMFmCH2nUQn5qGzKSQ-8U/edit?usp=sharing)
- [The Grbl Project](https://github.com/grbl/grbl/wiki)
- [Open Builds](https://openbuilds.com/)
- [Fabricatable Machines](https://github.com/fellesverkstedet/fabricatable-machines/wiki/Machine-family)
- [Trinamic SilentStepStick](https://learn.watterott.com/silentstepstick/comparison/)
- [Trinamic in Fablab Stock](https://www.digikey.fi/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626)
- [Stepper Shield](https://github.com/jw4rd/stepper)
- [Flexture Bot](https://github.com/jw4rd/flexureBot)
- [Lead Screw Linear Actuator](https://openbuilds.com/builds/v-slot-nema-23-linear-actuator-lead-screw.1393/)
- [Belt Drive Linear Actuator](https://openbuilds.com/builds/v-slot%C2%AE-nema-17-linear-actuator-belt-driven.80/)

---

## Machine

Proposed machine is a rotary table with various features to be decided by the team. Initial sketch? Next meeting Wed 16 Sep 15:00.

### TODO

- Identify usable parts from our stock.
- Identify missing electronic parts to be ordered.
- Try to make paper or cardboard prototype of the machine.
- Make a list of part to be designed and take measurements.
- Can the stepper shield work? Does it mill?
- Functionality of the application interface.
- First sketch of the interface and thoughts about user flow.

### Initial Responsibilities

- CAD / 3D Modelling of the parts: **Anssi**
- 3D Printing / CNC milling of the Parts: **Jasmine**
- PCB Milling and design if needed: **Ranjit**
- Control software / web interface and serial bridge: **Wan-Ting**

---

## Build

### PCB Milling and design: **Ranjit**

Tasks:

- PCB Milling and design
- Listed future development opportunities for this project
- 1-min Video

---

### Control software / web interface and serial bridge (Wan-Ting Hsieh)

#### Materials

- **Application Repository**: <https://gitlab.com/cv47522/nodejs-j5-arduino-stepper>
- Arduino UNO *1
- Stepper Driver ([TMC2208 SilentStepStick](https://www.digikey.fi/product-detail/en/trinamic-motion-control-gmbh/TMC2208-SILENTSTEPSTICK/1460-1201-ND/6873626) or [Pololu A4988](https://www.pololu.com/product/1182)) *1
- Stepper Motor (Bipolar, or 4-wire) *1
- 8-35V Power Adapter/Supply
- [5.5mm Barrel Power Jack](https://www.sparkfun.com/products/119) *1
- Optional: [DIY Stepper Shield](https://github.com/jw4rd/stepper) *1

#### Pin Connection (e.g. A4988)

![pololu](./images/Arduino_Stepper_Driver_TMC2208_pololu.jpg)

![fritzing](./images/Arduino_Stepper_Driver_TMC2208_fritzing.png)

#### Control Panel

##### Features

- **Stepper**
  - **RPM Speed**: sets the RPM speed of the motor (ideal range: **50-3000 rpm**, depending on the motor specification/datasheet)
  - **Direction**: sets the motor to move **clockwise** or **counterclockwise**

- **LED**
  - **Pulse Delay(ms)**: sets the blinkg speed of the LED

- **Monitor**: shows the respond after the user clicks a button or types a number

![panel](./images/panel.png)

#### How to design a web server with Node.js

**Learn more from [Week 14: Interface and Application Programming](https://fabacademy.wantinghsieh.com/assignment/14-interface-and-application-programming) of my website**

##### Installation

1. Install [Node.js](https://nodejs.org/en/download/)
2. Add and upload [AdvancedFirmata](https://github.com/soundanalogous/AdvancedFirmata) to Arduino ([installation](https://github.com/soundanalogous/AdvancedFirmata#to-use))
3. Open the terminal and run the below commands in order

    ``` bash
    git clone https://github.com/cv47522/nodejs-j5-arduino-stepper.git

    cd path/to/repository

    npm i  # install the required node modules automatically

    node webServer.js  # start the server
    ```

4. Type <http://localhost:3000> in a browser to go to the client-side control panel

#### Testing Video

{{< youtube y3VsqAG2W88 >}}

---

### Physical Prototyping - Jasmine

In coordination with Anssi, I (Jasmine) was able to successfully manufacture the analog part of the machine, through many iterations.

#### Designing the teeth

When designing the large gear, Anssi had to figure out the correct scale for the pulley/gear teeth, which are trapezoidal and require an uncommon gear generator. We used the laser to rapidly test different teeth sizes, and Anssi was able to determine that he needed to use the generated teeth at a 96% scale.

So close, yet so far!

![lasercut01](./images/lasercut_01.jpg)

#### 3D printing

This was the bulk of the manufacturing aspect - we initially chose 3D printing as our manufacturing process because we had a somewhat complex shape for our gears, as one had to have a groove inside for the bearings to rest in and both had to have guide walls on the outside, so we started with designing the big gear.

##### Big gear

Our first printed gear was 'close but not quite', teeth-alignment-wise, so Anssi redesigned the gear and I printed it, using a [LulzBot Mini](https://www.lulzbot.com/store/printers/lulzbot-mini) with a .8mm nozzle (chosen because the other printers with .4mm nozzles were already in use). The first gear had issues with the overhangs from the outer guides on the gear, so this one was printed with auto-generated supports. The path for the gear was wider than necessary, but the teeth fit the pulley very well.

![print01](./images/print_01.jpg)

![print02](./images/print_02.jpg)

![testing02](./images/testing_02.jpg)

##### Small gear and spacers

This was where we found out that our prints using the .4mm nozzle were exhibiting signs of shrinkage, when our spacers (essentially cylinders/thick washers) kept on printing with too-small measurements and not fitting on our screws, and the smaller gear design, meant to be installed on the shaft of the motor, consistently printed too small to fit. They also printed with a mysterious little brim, regardless of changes to the build plate adhesion settings in Cura.

I did not use the .8mm nozzle because it had such poor build plate adhesion for small prints that it consistently failed by not extruding but not sticking to the print bed, creating abstract blobs of plastic rather than useful spacers.

These are some test prints I did to test scaling - I made 3mm versions of the shaft and the spacers at 1x, 1.05x, 1.10x, and 1.10x scale, and had the frustrating experience of the spacers being adequately sized at 1.05x, while the shaft hole didn't fit until scaled to about 1.09x.

![print03](./images/print_03.jpg)

Another test print, where you can see the difficulty that our LulzBot .8mm has with prints sticking to the print bed, resulting in stringiness.
![print04](./images/print_04.jpg)

Krisjanis ended up doing some tests and found that, on our LulzBot Mini 1, scaling to 105% and reducing first layer flow to 90% remedied the printer issues, but the LulzBot Mini 4 needed even less flow. demonstrating the individual nature of different printers, despite being the same model/nozzle/build and using the same software.

I also printed some spacers and the motor gear on the [Ultimaker 2+ Extended](https://ultimaker.com/3d-printers/ultimaker-2-plus), but found similar shrinkage problems.

Going forward, I now know that each individual printer has its own quirks, so for very precise prints like our spacers, one should do some test prints to check for variations in accuracy.

We finally decided to print using the [Ultimaker 2+](https://ultimaker.com/3d-printers/ultimaker-2-plus)'s at Väre's 3D print workshop, as we suspected it would be more precise than the printers at our lab.

We had very successful prints; however, we have opted to use our laser cut gears instead as they are by default more precise (the prints still exhibited signs of very tiny shrinkage). The print files are included below.

#### Laser cutting

A while after discovering the scaling problems, Anssi redesigned the gears yet again so they could be laser cut as different layers and glued together, aligned by alignment holes and tabs that he built into the design. The files are linked below.

![lasercut02](./images/lasercut_02.jpg)

The resulting gears are currently being glued together, but some quick testing has shown them to be very promising, especially for accuracy, unlike the 3D prints.

![lasercut03](./images/lasercut_03.jpg)

#### Assembly

Here are some cross sections of the construction order:

Large gear + plate

![LargeGearAssembly](./images/LargeGearAssembly.jpg)

Motor gear

![MotorGearAssembly](./images/MotorGearAssembly.jpg)

Table

![TableMockup](./images/TableMockup.jpg)

Plate

![PlateAssembly](./images/PlateAssembly.jpg)

After some experimentation with washers and nuts to adjust heights, we were able to fully assemble the machine with minimal excess length on the screws and with secure connections.

#### Complete

Side view

![complete_01](./images/complete_01.jpg)

Top view

![complete_02](./images/complete_02.jpg)

### Files

[Gear 3D printer files, .stl format](images/Gear_Prints.zip)

[Gear laser files, .dxf format](images/Gears.zip)

[Table laser files, .dxf format](.images/Table.zip)

---
