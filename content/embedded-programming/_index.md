+++
title = "Week 08: Embedded Programming"
+++

Here you can find documentation of the Embedded Programming group assignment which maps to the following Fab Academy assignment weeks.

- [Embedded Programming](http://academy.cba.mit.edu/classes/embedded_programming/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Compare the performance and development workflows for different microcontroller families
- Document your work (in a group or individually)

---

### Initial Responsibilities

- AVR Programming - ATtiny 412 + avr-gcc + pyupdi + Terminal: **Wan-Ting**
- ESP32 Programming: **Jasmine**
- Arduino Programming: **Ranjit**
- ?: **Anssi**

---

## Build

---

### ESP32 Programming: **Jasmine**

---

### Arduino Programming: **Ranjit**

---

### [Wan-Ting: AVR Programming: ATtiny 412 + avr-gcc + pyupdi + Terminal](https://fabacademy.wantinghsieh.com/assignment/08-embedded-programming#group-assignment-compare-the-performance-and-development-workflows-between-different-avr-boards)

#### Step 1. Set up Programming Environment

I followed this [avr-gcc & avrdude makefile tutorial | Compile C & upload to AVR board (mac)](https://youtu.be/iKqLbbyPydI) and adjusted some parameters to generate **hex** code from a **elf** file (which is generated from a **c** file) via **avr-gcc** and then upload the **hex** file to the ATtiny 412 board via **avrdude**.

Follow the order below to install **avr-gcc** properly:

1. `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`:
  install Homebrew

2. `xcode-select --install`:
  install Xcode

3. `brew tap osx-cross/avr`

4. `brew install avr-gcc`: install [avr-gcc](https://github.com/osx-cross/homebrew-avr) & install [AVR Libc](https://www.nongnu.org/avr-libc/)

5. `brew install avrdude`: install [AVR Downloader/Uploader](https://www.nongnu.org/avrdude/)

#### Step 2. Start Programming **C** File

##### [hello.412.blink.c](https://gitlab.com/cv47522/avr-code/-/blob/master/ATtiny/c-code/hello.412.blink.c)

```c
#include <avr/io.h>
#include <util/delay.h>

int main(void) {
    PORTA.DIRSET = PIN3_bm;// or PORTA.DIR = 0b01000000; // Use LED: PA3 as Output
    PORTA.DIRCLR = PIN6_bm;// Use Button: PA6 as Input

    while (1) {
        PORTA.OUT |= LED_AVR;
        _delay_ms(500);
        PORTA.OUT &= ~LED_AVR;
        _delay_ms(500);
    }
}
```

##### Step 3. Create & Edit a **Makefile**

- **INCLUDE**, **PACK**: Download the add-on packs of latetest AVR chips from [Microchip Packs Repository](http://packs.download.atmel.com/) (**Atmel ATtiny Series Device Support** (1.8.332)), rename the file type from **.atpack** to **.zip** and unzip the pack to any diretory you like.

##### [Makefile](https://gitlab.com/cv47522/avr-code/-/blob/master/ATtiny/c-code/Makefile)

```makefile
FILENAME      = hello.412.blink
PORT 	      = /dev/tty.usbserial-D3072T1T
DEVICE 	      = attiny412
PYUPDI_DEVICE = tiny412
# PROGRAMMER   = jtag2updi
BAUD 	      = 115200
# For adding device packs of the latest chips (e.g. ATtiny412, 1614...)
INCLUDE       = "/Users/USER_NAME/Documents/AVR/Atmel.ATtiny_DFP.1.8.332/include"
PACK	      = "/Users/USER_NAME/Documents/AVR/Atmel.ATtiny_DFP.1.8.332/gcc/dev/attiny412"
COMPILE       = avr-gcc -Wall -Os -DF_CPU=20000000 -mmcu=${DEVICE} -I ${INCLUDE} -B ${PACK}

default: compile upload clean

compile:
	${COMPILE} -c ${FILENAME}.c -o ${FILENAME}.o
    //highlight-next-line
	${COMPILE} -o ${FILENAME}.elf ${FILENAME}.o  # Problem: Can't generate .elf from .o file.
	avr-objcopy -j .text -j .data -O ihex ${FILENAME}.elf ${FILENAME}.hex
	avr-size --format=avr --mcu=${DEVICE} ${FILENAME}.elf

upload:
	# avrdude -v -p ${DEVICE} -c ${PROGRAMMER} -P ${PORT} -b ${BAUD} -U flash:w:${FILENAME}.hex:i
	pyupdi -d ${PYUPDI_DEVICE} -b ${BAUD} -c ${PORT} -f ${FILENAME}.hex -v

clean:
	rm ${FILENAME}.o
	rm ${FILENAME}.elf
	rm ${FILENAME}.hex
```
![arduino_avr_gcc.png](./images/arduino_avr_gcc.png)

![elf.png](./images/elf.png)

#### Step 4. Upload the code via **Makefile**

Since the command for uploading the **hex** file to the ATtiny 412 board through **pyupdi** has been defined in the **upload** section of the **Makefile**, we just need to type the following commands in the terminal with the ATtiny 412 board connected to the UPDI programmer introduced [here](https://fabacademy.wantinghsieh.com/assignment/08-embedded-programming#set-up-programming-environment-megatinycore--pyupdi--arduino-ide).

```bash
cd path/to/hex/and/Makefile/file
make
```

![avr-gcc_folder.png](./images/avr-gcc_folder.png)
