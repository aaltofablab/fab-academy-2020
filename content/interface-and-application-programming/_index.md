+++
title = "Week 14: Interface and Application Programming"
+++

Here you can find documentation of the Interface and Application Programming group assignment which maps to the following Fab Academy assignment weeks.

- [Interface and Application Programming](http://academy.cba.mit.edu/classes/interface_application_programming/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Compare as many tool options as possible.

---

### Initial Responsibilities

- ?: **Anssi**
- ?: **Jasmine**
- ?: **Ranjit**
- Node.js (Socket.io, Express, Johnny-Five) + HTML + Bootstrap: **Wan-Ting**

---

## Build

### Node.js (Socket.io, Express, Johnny-Five) + HTML + Bootstrap ([Wan-Ting Hsieh's Documentation](https://fabacademy.wantinghsieh.com/assignment/14-interface-and-application-programming))

### ? (Jasmine)

### ? (Ranjit)
