+++
Title = "Fab Academy 2020 | Aalto Fablab"
+++

Welcome to the Fab Academy 2020 Aalto Fablab page. Here you can find information related to the lab, assignments and students.

## The Lab

Aalto Fablab is a digital fabrication facility of Aalto University. It was founded in 2012 as a part of the Aalto Media Factory. Now it is a part of the Aalto Studios and is located in a building in Otakaari 7, Espoo. Other workshops, such as set design, woodworking, metalworking, textile, AV editing, audio mixing, equipment takeout, VR and animation, are located in the same building and are available on request.

## Students

This year Fab Academy is an experiment for us. We have a continuing student, a student from Fab Lab Kerala and Aalto Media Lab students who are joining the Fab Academy to get credits for their master's degree. Group assignments are shared between them.

- [Solomon Kiflom](http://fabacademy.org/2019/labs/aalto/students/solomon-embafrash/) (Continuing)
- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/) (Fab Lab Kerala)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/) (Aalto Media Lab)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/) (Aalto Media Lab)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/) (Aalto Media Lab)

## Group Assignments

- [Week 03: Computer-Controlled Cutting](computer-controlled-cutting)
- [Week 04: Electronics Production](electronics-production)
- [Week 05: 3D Scanning and Printing](3d-scanning-and-printing)
- [Week 06: Electronics Design](electronics-design)
- [Week 07: Computer-Controlled Machining](computer-controlled-machining)
- [Week 08: Embedded Programming](embedded-programming)
- [Week 09: Input Devices](input-devices)
- [Week 11: Output Devices](output-devices)
- [Week 13: Networking and Communications](networking-and-communications)
- [Week 14: Interface and Application Programming](interface-and-application-programming)
- [Week 15: Molding and Casting](molding-and-casting)
- [Week 17: Mechanical and Machine Design](machine-building)
