+++
title = "Week 15: Molding and Casting"
+++

Here you can find documentation of the Molding and Casting group assignment which maps to the following Fab Academy assignment weeks.

- [Molding and Casting](http://academy.cba.mit.edu/classes/molding_casting/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Review the safety data sheets for each of your molding and casting materials
- Make and compare test casts with each of them

---

### Initial Responsibilities

- CAD / 3D Modelling of the parts: **Anssi**
- 3D Printing / CNC milling of the Parts: **Jasmine**
- PCB Milling and design if needed: **Ranjit**
- Control software / web interface and serial bridge: **Wan-Ting**

---

## Build

[Wan-Ting's Documentation](https://fabacademy.wantinghsieh.com/WeekSix)

---

## Files
