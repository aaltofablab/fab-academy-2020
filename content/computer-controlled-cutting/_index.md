+++
title = "Week 03: Computer-Controlled Cutting"
+++

This issue is related to Fab Academy [Week 03: Computer-Controlled Cutting](http://academy.cba.mit.edu/classes/computer_cutting/index.html).

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Characterise your laser cutter focus, power, speed, rate, kerf, and joint clearance

---

## Documented by [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/assignments/week-03/)

---

## Build

### characterized the laser cutter's with my group:

We have an Epilog Laser 36T, 60W laser cutter in our Fab Lab. The manual is available [here](https://www.epiloglaser.com/downloads/pdf/ext_4.22.10.pdf)

- The type of laser is CO2 Laser 60W.
- The cutting area is 914 x 610mm.
- The maximum height is 305mm.
- It supports the following file formats: .ai, .cdr, .pdf, and .svg.

It supports the following line weights when engraving:

![engraving01](./images/engraving_01.jpg)

The typical workflow for using the laser cutter  to cut is:

Software:

1. Save your vectorized image as an .ai file and bring to the laser cutter
2. Select all lines to be cut and set the stroke weight to 0.01mm and the color to R:255 (true red).
3. Open the print dialog, go to Setup, and go to Preferences to open the Epilog Dashboard
4. Select the Job Type, set the settings according to your local [guide](https://wiki.aalto.fi/display/AF/Laser+Cutter+Settings), correct the Piece Size, select Auto Focus, and Print. 
5. Select Print on the Preferences window, tick the "Ignore artboard" checkbox, ensure that the preview reflects a good place to print from, and select print.

At the laser:

- Follow this step-by-step [guide](https://wiki.aalto.fi/display/AF/Laser+Cutter+Epilog+Legend+36EXT).

#### focus

You can set the laser cutter to automatically focus the laser by ticking this box on the Epilog Dashboard:

![focus01](./images/focus_01.jpg)

It focuses at the first point of cutting. There is also an option to manually focus, which is available in the manual.

#### kerf

[Oskar](https://fabacademy.koli.io/) modeled a test for measuring kerf in [OpenSCAD](https://www.openscad.org/).

![kerf01](./images/kerf_01.jpg)

which included nine 20mm squares. We cut the file and measured each square (on both edges) and averaged the results, to come up with a measurement of 19.875mm. Subtracting that from the original spec of 20mm yielded a <b>kerf</b> of <b>0.125mm</b> - this means that when you're cutting a shape out, you need to add 0.125mm to the exterior of the shape to have an accurate cutout, and when you're cutting a hole out, you need to make the hole 0.125mm smaller all around to have an accurate cutout.

#### joint clearance

Oskar performed a joint clearance test using this file, cut out of 4mm plywood:

![joint01](./images/joint_01.jpg)

The settings were 20 speed/75 power/500 frequency.

These were his findings:

- 0.15mm = Attachable by hand. Very loose.
- 0.1mm = Attachable by hand. Stays together but not permanent
- 0.07mm = Attachable by hand with difficulty. Stays together, hard to take apart.
- 0.01mm = Attachable by hand with difficulty. Stays together, hard to take apart.

He mentioned that a low negative clearance could be used to get a permanent press-fit joint that would need to be hammered together.
