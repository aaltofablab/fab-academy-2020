+++
title = "Week 06: Electronics Design"
+++

This issue is related to Fab Academy [Week 06: Electronics Design](http://academy.cba.mit.edu/classes/electronics_design/index.html)

---

## Team

- [Ranjit Menon](http://fabacademy.org/2020/labs/kochi/students/ranjit-menon/)
- [Jasmine Xie](https://bluet87.gitlab.io/fab-academy/)
- [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/)
- [Anssi Alhopuro](https://anzibar.gitlab.io/fab-academy/)

---

## TODO

- Use the test equipment in your lab to observe the operation of a microcontroller circuit board (in minimum, check operating voltage on the board with multimeter or voltmeter and use oscilloscope to check noise of operating voltage and interpret a data signal)
- Document your work (in a group or individually)

---

## Documented by [Wan-Ting Hsieh](http://fabacademy.wantinghsieh.com/assignments/06-electronics-design)

---

## Build

### Debugging Tool: [Multimeter](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all)

After finishing soldering, I used a multimeter and switched it to the **buzzing mode** which sounds if two points are connected (**0Ω** between them). In order to find out how much current does each LED consume, I then measured their cross voltage by switching to the **DC voltage** mode:

#### Measure the Connection

{{< video src="PCB_measure_R-small.mp4" width="800px" >}}

#### Measure the Cross Voltage

{{< video src="PCB_measure_V-small.mp4" width="800px" >}}

The red dots between each component are multimeter probes. According to the statistic, I can calculate the exact current which go through both the LED and the resistor:

``` bash
Voltage across a resistor (V, volts) = Current through the resistor (A, amperes) * The Resistance of the resistor (Ω, ohms) <br />
→ V<sub>cc</sub> - V<sub>F</sub> = I<sub>F</sub> * R <br />
→ V<sub>R</sub> = I<sub>F</sub> * R <br />
→ 2.148V = I<sub>F</sub> * 499 Ω <br />
→ I<sub>F</sub> = 0.0043A = 4.3mA
```

#### V<sub>F</sub>: Cross Voltage of Power LED

![Sch_pwr_LED_V_measure.png](./images/Sch_pwr_LED_V_measure.png)

![PCB_measure_pwr_LED_V.png](./images/PCB_measure_pwr_LED_V.png)

#### V<sub>R</sub>: Cross Voltage of Resistor

![Sch_pwr_LED_R_V_measure.png](./images/Sch_pwr_LED_R_V_measure.png)

![PCB_measure_pwr_LED_R_V.png](./images/PCB_measure_pwr_LED_R_V.png)

Then I used the multimeter to verify my calculation and it seems that there is some difference between the theoretical number and the practical one when it comes to measuring small current which can be observed by an [oscilloscope](https://learn.sparkfun.com/tutorials/how-to-use-an-oscilloscope/all) in a more precise way:

#### LED OFF

![PCB_measure_pwr_LED_I_off.jpg](./images/PCB_measure_pwr_LED_I_off.jpg)

#### LED ON

![PCB_measure_pwr_LED_I_off.jpg](./images/PCB_measure_pwr_LED_I_on.jpg)
